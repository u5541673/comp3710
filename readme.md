	
    
1. Active the virtual environment: (For Windows) run: env\Scripts\activate.bat in termial 
2. Install the dependencies for virtual environment: run: pip install -r requirements.txt
3. Run Django server: run: python manage.py runserver in termial 
4. Open browser and go to http://127.0.0.1:8000/index 
- Do not kill the terminal during the experiment
- Data is stored at data/ 
- Data is saved automatically, close the website when the experiment is completed. 




