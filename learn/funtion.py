import random 
import os 
import numpy as np
LS = [
    [["FA","SG","HG","AA"],
    ["AG","HA","FA","SG"],
    ["HG","AA","SA","FG"],
    ["SA","FG","AG","HA"]],

    [["FA","SG","AG","HA"],
    ["SG","FA","HA","AG"],
    ["AA","HG","SA","FG"],
    ["HG","AA","FG","SA"]],

    [["AA","SG","FA","HG"],
    ["HG","AA","SG","FA"],
    ["SA","FG","HA","AG"],
    ["FG","HA","AG","SA"]],

    [["AA","SG","HG","FA"],
    ["HA","AG","FA","SG"],
    ["SA","FG","AG","HA"],
    ["FG","HG","SA","AA"]]
]

LS1 = [
    [3,4,0,2],
    [2,0,5,3],
    [1,5,2,0],
    [4,3,1,5],
    [0,2,4,1],
    [5,1,3,4]
]


    # for i in range(len(ls)):
    #     while 1:
    #         x= ["A","A","G","G"]
    #         random.shuffle(x)
    #         new_list = list( j + x.pop() for j in ls[i])
    #         if ("FA" in new_list and "SG" in new_list) or  ("FG" in new_list and "SA" in new_list):
    #             ls[i] = new_list
    #             break 

def hcc_video_sequence(ls):
    random.shuffle(ls)
    ls = ls [:3] # only keep the frist 3 groups 
    ls = (sum(ls , [] ))
    ls = (sum(ls , [] ))
    # for i in range(len(ls)):
    #     ls[i] = ls[i][::-1]

    dic_s = {
    "AG" : [1,2,3,4,5,6],
    "AA" : [1,2,3,4,5,6],
    "FG" : [1,2,3,4,5,6],
    "FA" : [1,2,3,4,5,6],
    "HG" : [1,2,3,4,5,6],
    "HA" : [1,2,3,4,5,6],
    "SG" : [1,2,3,4,5,6],
    "SA" : [1,2,3,4,5,6]
    }
    dic = dic_s.copy() 
    for i in dic:
        random.shuffle (dic[i])
        
    for i in range (len(ls)):
        ls[i] = ls[i][::-1]  +  str(dic[ls[i]].pop())
        
    return ls 

# def sequence_RFD(videos_path):



if __name__ == "__main__":
    # read all videos as six list 
    x = hcc_video_sequence(LS)
    print (x)
    Happy = [i[-1] for i in os.walk(r'C:\Users\gin19\Desktop\comp3710\Human_classification_emotional_faces-master\learn\static\learn\videos_RFD\Happy')][0]
    Anger = [i[-1] for i in os.walk(r'C:\Users\gin19\Desktop\comp3710\Human_classification_emotional_faces-master\learn\static\learn\videos_RFD\Anger')][0]
    Fear = [i[-1] for i in os.walk(r'C:\Users\gin19\Desktop\comp3710\Human_classification_emotional_faces-master\learn\static\learn\videos_RFD\Fear')][0]
    Sadness = [i[-1] for i in os.walk(r'C:\Users\gin19\Desktop\comp3710\Human_classification_emotional_faces-master\learn\static\learn\videos_RFD\Sadness')][0]
    Disgust = [i[-1] for i in os.walk(r'C:\Users\gin19\Desktop\comp3710\Human_classification_emotional_faces-master\learn\static\learn\videos_RFD\Disgust')][0]
    Surprised = [i[-1] for i in os.walk(r'C:\Users\gin19\Desktop\comp3710\Human_classification_emotional_faces-master\learn\static\learn\videos_RFD\Surprise')][0]
    
    # for i in range(len(Happy)):
    #     Happy[i] = "!!!!!!!!!!!!!1Happy-!!!!!!!!!!!!!!" + Happy[i]

    # count the max (can set 240 hardcoding)
    max_count = len(Happy)
    for emo in [Anger,Fear,Sadness,Disgust,Surprised]:
        back_up = emo.copy()
        random.shuffle(emo) # shuffle the first one 
        while 1:
            x = back_up.copy()
            # random.shuffle(x) # shuffle later 
            emo += x
            if len(emo) > max_count:
                break 

    np.random.permutation(range(1,7))[:4] # first 4 from numbers 1-6
    count = 100
    while Happy:
        emotions = [] # 48 videos 8 for each class 

        emotions.append(Happy[:8])
        random.shuffle(emotions[-1]) # shuffle the new list 
        Happy = Happy[8:]
        emotions.append(Anger[:8])
        random.shuffle(emotions[-1]) # shuffle the new list 
        Anger = Anger[8:]
        emotions.append(Fear[:8])
        random.shuffle(emotions[-1]) # shuffle the new list 
        Fear = Fear[8:]
        emotions.append(Sadness[:8])
        random.shuffle(emotions[-1]) # shuffle the new list 
        Sadness = Sadness[8:]
        emotions.append(Disgust[:8])
        random.shuffle(emotions[-1]) # shuffle the new list 
        Disgust = Disgust[8:]
        emotions.append(Surprised[:8])
        random.shuffle(emotions[-1]) # shuffle the new list 
        Surprised = Surprised[8:]

        # for emo in [Happy,Anger,Fear,Sadness,Disgust,Surprised]:
        #     print(len(Happy))
        #     try:
        #         emotions.append(emo[:8])
        #         random.shuffle(emotions[-1]) # shuffle the new list 
        #         emo = emo[8:]
        #     except:# done 
        #         print ("done")
        #         break 
        
        subject_sequence = [] 

        # hardcode as 48 for each subject 
        index_sequence_48 = []  # generate a 48 index sequence, eg 1,2,3,4,5,6, 
        random.shuffle(LS1)
        for i in range (len(LS1)):
            random.shuffle(LS1[i])
            index_sequence_48 += LS1[i]
        for i in range (len(LS1)):
            random.shuffle(LS1[i])
            index_sequence_48 += LS1[i]

        for i in index_sequence_48: # i from 0-5, represents 6 types of emo 
            subject_sequence.append(emotions[i].pop())
        count += 1
        print (count)
        file_name = 'data/sequence/'+ str(count) + '.txt'
        # file_name = "C:/Users/gin19/Desktop/comp3710/Human_classification_emotional_faces-master/data/sequence/" + str(count) + '.txt'

        with open (file_name , 'w') as f:
            c = 0 
            hcc_first = True
            hcc = hcc_video_sequence(LS)
            for s in range(len(subject_sequence)):
                c += 1 
                # if (c % 8 == 0) :  
                #     if hcc_first:
                #         hcc_first = False
                #     else:
                #         hcc_first = True
                if hcc_first: 
                    f.write(subject_sequence[s])
                    f.write(",")
                    f.write(hcc[s])
                else:
                    f.write(hcc[s])   
                    f.write(",")
                    f.write(subject_sequence[s])            
                if s < len(subject_sequence) - 1:
                    f.write(",") 
                


    

    


