from django import forms
from .models import Questionnaire, ExperimentPage, ExperimentQuestionPage, AfterExperiment
from django.utils.translation import ugettext_lazy as _

class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)
    
class Info1(forms.Form):
#    your_name = forms.CharField(label='Your name', max_length=100)
#    MEDIA_CHOICES = (('Audio', (('vinyl', 'Vinyl'),('cd', 'CD'),)),('Video', (('vhs', 'VHS Tape'),('dvd', 'DVD'),)),('unknown', 'Unknown'),)
    gender_choices = [('m' , 'Male') , ('f','Female'),('unkown','Not disclosed')]
    glasses_choice = [('y','Yes') , ('n','No')]
    
    ege = forms.IntegerField(min_value = 1)
    sex = forms.ChoiceField(choices = gender_choices)
    glasses = forms.ChoiceField(choices = glasses_choice)
#    sex = forms.ChoiceField(MultipleChoiceField)

#class Display(forms.Form):
#    your_name = forms.CharField(label='Your name', max_length=100)
#    
#    date_id = models.IntegerField(primary_key = True)
#    answer = models.TextField()
#    name = models.TextField()
#    other_info = models.TextField()
#    time_stamp = models.TextField()




class QuestionnaireForm(forms.ModelForm):

    class Meta:
        model = Questionnaire
        fields = ['age', 'glass','gender', 'ethnicity', 'education', 'major', 'language', 'music_time_spent', 'music_genre', 'music_play', 'migraine']
        labels = {
            'ethnicity': _('How would you classify yourself?'),
            'glass': _('Are you currently wearing glasses or contacts?'),
            'education': _('What level of education have you completed (or currently completing)?'),
            'major': _('What is/was your major/field of study?'),
            'language': _('What is your native language?'),
            'music_time_spent': _('How much time do you spend listening to music every day?'),
            'music_genre': _('What type of music do you mostly listen to?'),
            'music_play': _('Do you play any instrument? If yes then please mention the instruments you play.'),
            'migraine': _('Do you have any history of Migraine/Severe Headache?'),
        }
        widgets = {
        #  'ethnicity': forms.SelectMultiple(attrs={'required': 'true'}),
        #  'ethnicity': forms.CheckboxSelectMultiple(),
         }

class ExperimentForm(forms.ModelForm):
    class Meta:
        model = ExperimentPage
        fields = ['song_ref', 'text_ref', 'song_volume']


class ExperimentQuestionForm(forms.ModelForm):
    class Meta:
        model = ExperimentQuestionPage
        fields = ['q1', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'q11']
        labels = {
            'q1': _('Have you heard this piece before?'),
            'q4': _('How would you describe the music you have just heard?'),
            'q8': _('While the music was playing, how much attention did you give to the music?'),
            'q9': _('You found the music--'),
            'q10': _('The music made you feel more--'),
            'q11': _('Why did you like/dislike this piece?'),

        }
        widgets = {
        #  'q1': forms.RadioSelect(attrs={'required': 'true'}),
         'q4': forms.RadioSelect(attrs={'required': True}),
         'q5': forms.RadioSelect(attrs={'required': True}),
         'q6': forms.RadioSelect(attrs={'required': True}),
         'q7': forms.RadioSelect(attrs={'required': True}),
         'q8': forms.RadioSelect(attrs={'required': True}),
         'q9': forms.RadioSelect(attrs={'required': True}),
         'q10': forms.RadioSelect(attrs={'required': True}),
         }


class AfterExperimentForm(forms.ModelForm):
    class Meta:
        model = AfterExperiment
        fields = ['e1']
        labels = {
            'e1': _('If any of the video or music pieces made you feel uncomfortable, why did you feel so?'),

        }
