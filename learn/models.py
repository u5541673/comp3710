# Create your models here.
from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from multiselectfield import MultiSelectField
# class Employee(models.Model):
	# ID
    # name=models.CharField(max_length=20)
	 
	 
	 
# Create your models here.
class historical_answer(models.Model):
    date_id = models.IntegerField(primary_key = True)
    answer = models.TextField()
    name = models.TextField()

class all_info(models.Model):
    date_id = models.IntegerField(primary_key = True)
    answer = models.TextField()
    name = models.TextField()
    other_info = models.TextField()
    time_stamp = models.TextField()

class completed_info_hcc(models.Model):
    date_id = models.IntegerField(primary_key = True)
    name = models.TextField()
    language = models.TextField()
    ethnicity = models.TextField(blank=False)
    glassess = models.TextField()
    age = models.TextField()
    gender = models.TextField()
    questions = models.TextField()
    answer = models.TextField()
    confidence = models.TextField()
    seen_before = models.TextField()

class sessions(models.Model):
    session_id = models.TextField(primary_key = True) # <int a>_<int b> .a is the count of full-dataset ; b is the session ID in this full-dataset 
    video_set =  models.TextField()
    is_watched = models.BooleanField(default= False) # 0 unwatched 1: watched
    
class afew_experiment_session(models.Model):
    date_id = models.IntegerField(primary_key = True)
    uid = models.TextField() # only required for full_set people
    is_full_dataset = models.BooleanField() # double check 
    name = models.TextField()
    language = models.TextField()
    ethnicity = models.TextField(blank=False)
    glassess = models.TextField()
    age = models.TextField()
    gender = models.TextField()
    questions = models.TextField()
    answer = models.TextField()
    confidence = models.TextField()
    seen_before = models.TextField()
    time_stamp = models.TextField()
    erq = models.TextField(default = '')
    
class person_full_set(models.Model):
    uid = models.IntegerField(primary_key = True)
    emotions_if_watched = models.TextField()



EDUCATION_CHOICES = (
    ('highschool', 'High School'),
    ('tafe', 'Diploma \ Grad. cert.'),
    ('undergrad', 'Undergradute Degree'),
    ('master', 'Masters Degree'),
    ('phd', 'PhD'),
    ('other', 'Other'),
)

GENDER_CHOICES = (
    ('female', 'Female'),
    ('male', 'Male'),
    ('other', 'I would prefer not to say'),
)

ETHNICITY_CHOICES = (
    ('asian', 'Asian '),
    ('hispanic', 'Hispanic '),
    ('latino', 'Latino '),
    ('indigenous', 'Indigenous '),
    ('white', 'Caucasian/White '),
    ('black', 'Black '),
    ('other', 'Other '),
    ('no', 'I would prefer not to say '),

)
MIGRAINE_CHOICES = (
    ('yes', 'Yes'),
    ('no', 'No'),
)
READING_CHOICES = (
    ('1', 'Easy'),
    ('2', 'Moderately Easy'),
    ('3', 'Somewhat easy'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Difficult'),
    ('6', 'Moderately Difficult'),
    ('7', 'Difficult'),
)
COMFORT_CHOICES = (
    ('1', 'Comforting'),
    ('2', 'Moderately Comforting'),
    ('3', 'Somewhat Comforting'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Disturbing'),
    ('6', 'Moderately Disturbing'),
    ('7', 'Disturbing'),
)

RELAX_CHOICES = (
    ('1', 'Relaxed'),
    ('2', 'Moderately Relaxed'),
    ('3', 'Somewhat Relaxed'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Tensed'),
    ('6', 'Moderately Tensed'),
    ('7', 'Tensed'),
)

DEPRESS_CHOICES = (
    ('1', 'Depressing'),
    ('2', 'Moderately Depressing'),
    ('3', 'Somewhat Depressing'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Exciting'),
    ('6', 'Moderately Exciting'),
    ('7', 'Exciting'),
)

HAPPY_CHOICES = (
    ('1', 'Happy'),
    ('2', 'Moderately Happy'),
    ('3', 'Somewhat Happy'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Sad'),
    ('6', 'Moderately Sad'),
    ('7', 'Sad'),
)

UNPLEASANT_CHOICES = (
    ('1', 'Unpleasant'),
    ('2', 'Moderately Unpleasant'),
    ('3', 'Somewhat Unpleasant'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Pleasant'),
    ('6', 'Moderately Pleasant'),
    ('7', 'Pleasant'),
)

ATTENTION_CHOICES = (
    ('1', 'No Attention'),
    ('2', 'Very Low Attention'),
    ('3', 'Low Attention'),
    ('4', 'Neutral'),
    ('5', 'Some Attention'),
    ('6', 'Moderate Attention'),
    ('7', 'Listened Attentively'),
)

IRRITATION_CHOICES = (
    ('1', 'Irritating'),
    ('2', 'Moderately Irritating'),
    ('3', 'Somewhat Irritating'),
    ('4', 'Neutral'),
    ('5', 'Somewhat Soothing'),
    ('6', 'Moderately Soothing'),
    ('7', 'Soothing'),
)
# Sequences are hard coded based on Latin square generation of sequences.
# The first part is the ordering of images: H = heirarchy, R = radial and S = Standard
SEQUENCES = (
    ('1','H,R - 1,2,6,3,5,4'),
    ('2','H,R - 2,3,1,4,6,5'),
    ('3','H,R - 3,4,2,5,1,6'),
    ('4','H,R - 4,5,3,6,2,1'),
    ('5','H,R - 5,6,4,1,3,2'),
    ('6','H,R - 6,1,5,2,4,3'),
    ('7','R,S - 1,2,6,3,5,4'),
    ('8','R,S - 2,3,1,4,6,5'),
    ('9','R,S - 3,4,2,5,1,6'),
    ('10','R,S - 4,5,3,6,2,1'),
    ('11','R,S - 5,6,4,1,3,2'),
    ('12','R,S - 6,1,5,2,4,3'),
    ('13','S,H - 1,2,6,3,5,4'),
    ('14','S,H - 2,3,1,4,6,5'),
    ('15','S,H - 3,4,2,5,1,6'),
    ('16','S,H - 4,5,3,6,2,1'),
    ('17','S,H - 5,6,4,1,3,2'),
    ('18','S,H - 6,1,5,2,4,3'),
)


# Pre-experiment questionnaire
class Questionnaire(models.Model):
    timestamp = models.DateTimeField('Time Collected', auto_now_add=True)
    age = models.IntegerField()
    glass = models.CharField(choices=MIGRAINE_CHOICES, max_length=100)
    gender = models.CharField(choices=GENDER_CHOICES, max_length=100)
    ethnicity = MultiSelectField(choices=ETHNICITY_CHOICES)
    education = models.CharField(choices=EDUCATION_CHOICES, max_length=100)
    major = models.CharField(max_length=100)
    language = models.CharField(max_length=100)
    migraine = models.CharField(choices=MIGRAINE_CHOICES, max_length=100)
    music_time_spent = models.CharField(max_length=100)
    music_genre = models.CharField(max_length=100)
    music_play = models.CharField(max_length=100)
    

# Form for each experiment page
class ExperimentPage(models.Model):
    timestamp = models.DateTimeField('Time Collected', auto_now_add=True)
    song_ref = models.CharField(max_length=100)
    text_ref = models.CharField(max_length=100)
    song_volume = models.DecimalField(max_digits=20, decimal_places=17, default=Decimal('0.0000'))


# Form for each experiment page
class ExperimentQuestionPage(models.Model):
    timestamp = models.DateTimeField('Time Collected', auto_now_add=True)
    q1 = models.CharField(choices=MIGRAINE_CHOICES, max_length=100)
    # q2 = models.CharField(choices=MIGRAINE_CHOICES, max_length=100)
    # q3 = models.CharField(choices=READING_CHOICES, max_length=100)
    q4 = models.CharField(choices=COMFORT_CHOICES, max_length=100)
    q5 = models.CharField(choices=DEPRESS_CHOICES, max_length=100)
    q6 = models.CharField(choices=HAPPY_CHOICES, max_length=100)
    q7 = models.CharField(choices=UNPLEASANT_CHOICES, max_length=100)
    q8 = models.CharField(choices=ATTENTION_CHOICES, max_length=100)
    q9 = models.CharField(choices=IRRITATION_CHOICES, max_length=100)
    q10 = models.CharField(choices=RELAX_CHOICES, max_length=100)
    q11 = models.CharField(max_length=100)
    # q12 = models.CharField(max_length=100)


# Questions at the end of experiment
class AfterExperiment(models.Model):
    timestamp = models.DateTimeField('Time Collected', auto_now_add=True)
    e1 = models.CharField(max_length=400)


# table of questions asked to participants with the associated image and answer
class Questions(models.Model):
    question_text = models.CharField(max_length=200)
    question_number = models.IntegerField()
    song_ref = models.CharField(max_length=200)
    answer = models.CharField(max_length=200)


# Keep track of the sequences that have already been
class Sequences(models.Model):
    sequence = models.CharField(choices=SEQUENCES, max_length=100, unique=True)
    tally = models.IntegerField(default=0)

# class sequence_count(models.Model):
    
