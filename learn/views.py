from django.shortcuts import render
from django.urls import reverse 
from django.http import HttpResponse, Http404 , HttpResponseRedirect
from django.contrib import messages
import numpy as np
import random 
import collections
import time
import json
from functools import reduce
import os 

from .forms import QuestionnaireForm, ExperimentForm, AfterExperimentForm, ExperimentQuestionForm
# from .models import Questionnaire, Questions, ExperimentPage, Sequences, AfterExperiment, ExperimentQuestionPage


#Hyperparameter, to control the experiment process, mainly for testing 
quick_test = 0 # 1: play the videos directly
skip_confidence = 0 # 1 skip condifence questions
music_fast_mode = 0 
videos_each_block = 8 

# global variables used to get access to the local variables of functions
# an elegant solution is put those temporary data into database with a fixed primary key 
# reload them when use them. 
all_global_vars = {'sequence' : [] , 'current_page' : 0 , 'sequence_music' :[] ,'music_count':0 , 'sequence_file' : ""}  # sequences and counter for music and video; a way to comminucate between view funtions and normal funtion; alternative for .session 
initial_time = "" # record the exp begin time, used for naming output file 
music_data = [] # store music_data answer, time start, music name, answer1-12
video_data = [] # store video_data answer, time start, video name, G/A, seen before, time end 
music_question = False
pre_quesiton = False

def test(request):
    global all_global_vars 
    
    all_global_vars["music_count"] += 1 
    print (all_global_vars["music_count"])
    return render(request,'learn/Intro.html')

def get_next_video ():
    global all_global_vars 
    # try:
    #     emo , index = all_global_vars['sequence'].pop(0).split('_')
    # except:
    #     print("run out of videos....")
    #     return "no_video"
    # if emo == '1':
    #     path_emotion = 'A' #anger video_index > 10 --> acted ; 0-10 ---> genuine
    #     q_emo = 'Anger'
    # elif emo == '2':
    #     path_emotion = 'H' # happiness
    #     q_emo = 'Happiness'
    # elif emo == '3':
    #     path_emotion = 'S' # surprise
    #     q_emo = 'Surprise'
    # elif emo == '4':
    #     path_emotion = 'F' #fear
    #     q_emo = 'Fear'
    # if int (index) > 10:
    #     index = str (int(index) - 10) # index > 10 --> acted 
    #     path_authenticity = 'A'
    # elif int(index) <= 10:
    #     path_authenticity = 'G'
    # all_global_vars['current_page'] += 1 
    # path = 'static/learn/videos/' + path_authenticity + path_emotion + index + '.mp4'
    all_global_vars['current_page'] += 1 
    path = 'static/learn/videos/' + all_global_vars['sequence'].pop(0)
    print ( "current_video: #" + str (all_global_vars['current_page']) + "file: " +  path)
    return path 

def get_next_music():
    global all_global_vars 
    m_index , _class = all_global_vars['sequence_music'].pop(0).split('_') # like 1_2; should be 01_pop # correction: first one should alwasy be 1-4, only the second one is random
    m_index = "0" + m_index

    if _class =="1":
        m_class = "_classical.mp3"
    if _class =="2":
        m_class = "_instrumental.mp3"
    if _class =="3":
        m_class = "_pop.mp3"
    music_path = 'static/learn/songs/' + m_index + m_class
    print ("current_music: " + music_path)
    return music_path

def return_emotions(emo_code):
    #case: 1: Anger 2: Happiness 3: Surpise 4: Fear
    if emo_code == '1':
        return 'Anger'
    elif emo_code == '2':
        return 'Happiness'
    elif emo_code == '3':
        return 'Surprise'
        path_emotion = 'S' # surprise
        q_emo = 'Surprise'
    elif emo_code == '4':
        return 'Fear'
        path_emotion = 'F' #fear
        q_emo = 'Fear'
        
def initialize():
    # ensure all global varibales are initialized.
    global all_global_vars , music_data , video_data ,initial_time , music_question , pre_quesiton
    all_global_vars = {'sequence' : [] , 'current_page' : 0 , 'sequence_music' :[] ,'music_count':0 , 'sequence_file' : ""}
    initial_time = str(int(time.time()))
    music_data = []
    video_data = []
    music_question =  pre_quesiton = False

def write_last_line(data,cls):
    global initial_time
    file_name = 'data/backup/'+initial_time+'_'+cls+'.txt'
    

    try:
        with open (file_name , 'a+') as f:
            if type(data[-1]) == list: 
                line = data[-1].copy()
                f.write(str(line.pop(0)))
                while line:
                    f.write(';')# no ";" in the end eachline 
                    f.write (str(line.pop(0)))
                f.write('\n')
            else: # a string or something
                f.write(str(data[-1]))
    except FileNotFoundError:
        # create file 
        print ("{} : file created".format(file_name))
        fd = open(file_name,'w')
        fd.close()
        write_last_line(data,cls)

def generate_sequence():
    # one sequence for emotions # another for videos for each emotions
    global all_global_vars
    # emotion = np.random.permutation(range(1,5)) # [1-4] randomly 
    # videos = [np.random.permutation(range(1,21)) for i in range(4)]
    # for i in range(len(videos)):
    #     for j in range(len(videos[0])):
    #         all_global_vars['sequence'].append(str(emotion[i]) + '_' + str(videos[i][j])) # generate the sequence 
    # print("-------------------------- video sequence is: --------------------------")
    # print (all_global_vars['sequence'])

    ### video sequence
    # find the sequence file 
    # all_files = [i[-1] for i in os.walk('data/sequence/')][0]
    # files = [ int(i.split(".")[0]) for i in all_files]
    # sequence_file = 'data/sequence/' + str(min(files)) + ".txt"
    # all_global_vars["sequence_file"] = sequence_file
    # load the text file 

    #
    with open ('data/sequence/sq.txt') as f:
        s = f.readline()
        sequence_file = 'data/sequence/' + str(s) + ".txt"
        all_global_vars["sequence_file"] = str(s)
    print ("sequence file is loaded: " + sequence_file)
    with open (sequence_file , 'r') as f:
        s = f.readline()
        sequence = s.split(",")
        for i in range (len(sequence)):
            if "mp4" not in sequence[i]:
                sequence[i] += ".mp4"
        all_global_vars['sequence'] = sequence
    print (all_global_vars['sequence'])
    print (len (all_global_vars['sequence']) )
    # check if all videos are exist in the folder  

    # music sequence 
    # song_index = np.random.permutation(range(1,5)) # X_1-4
    song_class = np.random.permutation(range(1,4)) # 1-3_X
    for j in range(len(song_class)):
        for i in range(1,5):
            all_global_vars['sequence_music'].append(str(i) + '_' + str(song_class[j]))
    print("-------------------------- music sequence is: --------------------------")
    print(all_global_vars['sequence_music'])

def index(request):
    initialize()
    generate_sequence()
    return render(request,'learn/index.html')

def intro_page(request):
    global quick_test 
    if quick_test:
        return HttpResponseRedirect('playing')      
    return render(request,'learn/Intro.html')

def intro_page2(request):
    return render(request,'learn/introtips.html')

def questionnaire(request):
    # if this is a POST request we need to process the form data
    global pre_quesiton
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = QuestionnaireForm(request.POST)
        global video_data, all_global_vars
        # check whether it's valid:
        if form.is_valid():
            pre_quesiton = False
            # process the data in form.cleaned_data as required
            age = form.cleaned_data['age']
            glass = form.cleaned_data['glass']
            ethnicity = form.cleaned_data['ethnicity']
            education = form.cleaned_data['education']
            gender = form.cleaned_data['gender']
            migraine = form.cleaned_data['migraine']
            major = form.cleaned_data['major']
            language = form.cleaned_data['language']
            music_time_spent = form.cleaned_data['music_time_spent']
            music_genre = form.cleaned_data['music_genre']
            music_play = form.cleaned_data['music_play'] # questionnaire_form = form.save()
            video_data.append([age,gender,glass,education,major,ethnicity,language,migraine,music_time_spent,music_genre,music_play ,all_global_vars["sequence_file"]])
            write_last_line(video_data,"video")
            # # break check 
            # if all_global_vars["music_count"] % 3 == 0 and all_global_vars["music_count"] > 0:
            #     return HttpResponseRedirect('break')  
            return HttpResponseRedirect('playing')     
        else:
            # invalid data: 
            pre_quesiton = True
    else:
        form = QuestionnaireForm()
    return render(request, 'learn/questionnaire.html', {'form': form , "pre_question" : pre_quesiton })


        # else:
        #     age = form['age']
        #     glass = form['glass']
        #     ethnicity = form['ethnicity']
        #     education = form['education']
        #     gender = form['gender']
        #     migraine = form['migraine']
        #     major = form['major']
        #     language = form['language']
        #     music_time_spent = form['music_time_spent']
        #     music_genre = form['music_genre']
        #     music_play = form['music_play'] # questionnaire_form = form.save()

def playing (request):
    print ("view.playing is running..................................................")
    global all_global_vars , music_data , video_data
    t = str(int(time.time()))
    # if run out of videos/music 
    if len (all_global_vars["sequence"]) == 0: # run out of videos finish all videos turn to erq
        return HttpResponseRedirect('music_post_question')
    if len (all_global_vars["sequence_music"]) == 0: # run out of music finish all videos turn to erq
        return HttpResponseRedirect('music_post_question')
    path = get_next_video()
    
    path_music = get_next_music()
    
    all_global_vars["music_count"] += 1 # used for break check, ask subject do they need a break every three music clips 
    music_data.append([os.path.basename(path_music),t]) # first music data for this line 
    video_data.append([os.path.basename(path), t]) # first video data for this line 
    last_video_block = "true" if all_global_vars['current_page'] % 8 == 0 else "false"
    return render(request,'learn/playing.html',{'path': path , 'path_music': path_music ,  'last_video_block' : last_video_block , 'music_fast_mode' :music_fast_mode })

def get_ajex(request):
    global all_global_vars , video_data
    t = str(int(time.time()))
    if request.method == 'GET':
        a = request.GET["authenticity"]
        video_data[-1] += [a , t] # add time stamp here, the time video ends
        return HttpResponse(str(a))

def save_confidence(request):
    # save confidence choices and read/generate then return next video path
    global all_global_vars , video_data
    if request.method == 'GET':
        try:
            a = request.GET["confidence"]
            b = request.GET["seen_before"]
        except:
            a = b = "error"
        t = str(int(time.time()))
        video_data[-1]+= [a , b] # this video is end 
        write_last_line(video_data,"video") # start a new line, write the last line before it 
        try: 
            if all_global_vars['current_page'] % (videos_each_block) != 0:
                path = get_next_video() # next video 
                video_data.append([os.path.basename(path), t]) # start a new line for this video 
            else:
                all_global_vars['current_page'] += 1 
                path = "block_over"
        except:
            path = "block_over"


        return HttpResponse(path)

def music_questions(request):
    global all_global_vars , music_question
    # render and save the music quesitons 
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ExperimentQuestionForm(request.POST)
        # check whether it's valid:
        if form.is_valid(): # process the data in form.cleaned_data as required
            music_question = False
            q1 = form.cleaned_data['q1']
            # q2 = form.cleaned_data['q2']
            # q3 = form.cleaned_data['q3']
            q4 = form.cleaned_data['q4']
            q5 = form.cleaned_data['q5']
            q6 = form.cleaned_data['q6']
            q7 = form.cleaned_data['q7']
            q8 = form.cleaned_data['q8']
            q9 = form.cleaned_data['q9']
            q10 = form.cleaned_data['q10']
            q11 = form.cleaned_data['q11']
            # q12 = form.cleaned_data['q12']

        # save data 
            global music_data 
            music_data[-1] += [q1,q4,q5,q6,q7,q8,q9,q10,q11,str(int(time.time()))] # a music line data is over 
            write_last_line(music_data,"music") 
            print(music_data)
            if all_global_vars["music_count"] == 6:
                return HttpResponseRedirect(reverse('break'))
            return HttpResponseRedirect(reverse('playing'))
        else:
            messages.info(request, 'Your password has been changed successfully!')
            music_question = True
    else:
        form = ExperimentQuestionForm()
    
    return render(request, 'learn/expquestions.html', {'form': form , "music_count" : all_global_vars["music_count"] , "music_question" : music_question})

        # else:
        #     q1 = form['q1']
        #     # q2 = form['q2']
        #     # q3 = form['q3']
        #     q4 = form['q4']
        #     q5 = form['q5']
        #     q6 = form['q6']
        #     q7 = form['q7']
        #     q8 = form['q8']
        #     q9 = form['q9']
        #     q10 = form['q10']
        #     q11 = form['q11']
        #     # q12 = form['q12']

def breakpage (request):
    return HttpResponseRedirect(reverse('playing'))
    # return render (request,'learn/break.html') 
    
def erq (request):
    # render the ERQ Questionnaire page and save the answers 
    if request.method == "POST":
        global video_data
        x= [] 
        for i in range(1,11):
            dir_name = 'Item' + str(i)
            x += request.POST[dir_name]
        video_data.append(x)
        write_last_line(video_data,"video") 
        return HttpResponseRedirect('end')
    else:
        return render(request, 'learn/ERQ.html')

def music_post_question(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AfterExperimentForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            e1 = form.cleaned_data['e1']
            global music_data 
            music_data.append (e1)  # a music line data is over 
            write_last_line(music_data,"music") 
            return end (request)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AfterExperimentForm()
    return render(request, 'learn/expend.html', {'form': form})



def end(request):
    global music_data , video_data , all_global_vars
    # save all video and music data and render the thanks page 
    # save video data:
    t = str(int(time.time()))
    file_name = 'data/'+ t + '_video.txt'
    with open (file_name , 'w') as f:
        for line in video_data:
            if type (line) == list:
                f.write(str(line.pop(0)))
                while line:
                    f.write(';')# no ";" in the end eachline 
                    data = line.pop(0)
                    if type (data) == list:
                        for i in data:
                            f.write(i)
                            f.write("|")
                    else:
                        f.write (data)
                f.write('\n')
            else:
                f.write(line)
                f.write('\n')

    file_name = 'data/'+ t + '_music.txt'
    with open (file_name , 'w') as f:
        for line in music_data:
            if type (line) == list:
                f.write(str(line.pop(0)))
                while line:
                    f.write(';')# no ";" in the end eachline 
                    data = line.pop(0)
                    if type (data) == list:
                        for i in data:
                            f.write(i)
                            f.write("|")
                    else:
                        f.write (data)
                f.write('\n')
            else:
                f.write(line)
                f.write('\n')            
    # move the sequence file to /used 
    # os.rename(all_global_vars["sequence_file"], 'data/sequence/used/' + os.path.basename(all_global_vars["sequence_file"]))

    with open ('data/sequence/sq.txt' , 'w') as f:
        f.write(  str (int(all_global_vars["sequence_file"]) + 1) )

    return render (request,'learn/Conclusion.html')

# def info_collection_p2(request):
    # return render(request, 'learn/survey2.html')

# def info2_add(request):
#     if request.method =='POST':
#         Ethnicity = request.POST["Ethnicity"]
#         Language = request.POST["Language"]
#         OtherLanguage = request.POST["OtherLanguage"]
#     global all_global_vars    
#     all_global_vars['other_info'].append(Ethnicity)
#     all_global_vars['other_info'].append(Language)
#     all_global_vars['other_info'].append(OtherLanguage)
#     all_global_vars['personal_info']['ethnicity'] = Ethnicity
#     all_global_vars['personal_info']['Language'] = [Language,OtherLanguage]

#     return HttpResponseRedirect('emojudgement_Conclusion/result')     

# def cal_accuracy(correct,count):
#     try:
#         return format (correct/count , '.0%')
#         print(correct,count)
#     except ZeroDivisionError:
#         print(correct,count)
#         return 'N/A'
    
# def result_display(request):
#     #analyze and display the result
#     global all_global_vars
#     all_global_vars['user_name'] += '\'s'
#     print(all_global_vars['videos_answer'])
#     print(all_global_vars['user_name'])
#     h_count = f_count = a_count = s_count = 0
#     h_correct = f_correct = a_correct = s_correct = 0
#     output = collections.defaultdict(lambda : 0)
    
#     #count the correct and incorrect answers 
#     for emo,actual,answer in all_global_vars['videos_answer']:
#         if emo == 'H':
#             output ['h_count'] += 1 
#             if actual == answer:
#                 output['h_correct'] += 1              
#         elif emo == 'F':
#             output['f_count'] += 1
#             if actual == answer:
#                 output['f_correct'] += 1 
#         elif emo == 'A':
#             output['a_count'] += 1
#             if actual == answer:
#                 output['a_correct'] += 1
#         elif emo == 'S':
#             output['s_count'] += 1
#             if actual == answer:
#                 output['s_correct'] += 1 
                
#     # get the accuracy 
#     smile_accuracy = cal_accuracy (output['h_correct'] , output['h_count'])
#     anger_accuracy = cal_accuracy (output['a_correct'] , output['a_count'])
#     fear_accuracy = cal_accuracy (output['f_correct'] , output['f_count'])
#     suprise_accuracy = cal_accuracy (output['s_correct'] , output['s_count'])
    

#     # read the privous answers and cal the average accuracy 
#     # filter out test datauser_name == test 
#     record = historical_answer.objects.all().values('answer')
#     if record:
#         historical_count = [collections.Counter(json.loads (i['answer'])) for i in record]
#         historical_count_total = reduce(lambda x,y:x+y,historical_count)
#     print(historical_count_total)            
#     smile_accuracy_average = cal_accuracy (historical_count_total['h_correct'] , historical_count_total['h_count'])
#     anger_accuracy_average = cal_accuracy (historical_count_total['a_correct'] , historical_count_total['a_count'])
#     fear_accuracy_average = cal_accuracy (historical_count_total['f_correct'] , historical_count_total['f_count'])
#     suprise_accuracy_average = cal_accuracy (historical_count_total['s_correct'] , historical_count_total['s_count'])            
    
#     #save this participant's data into database 
#     historical_answer.objects.create(
#         date_id = int(time.time()),
#         answer = json.dumps(output),
#         name = all_global_vars['user_name'][:-2]
#     )
    
#     #save this participant's data into database 
#     try:
#         print('all info:')
#         print(output)
#         print(all_global_vars['user_name'][:-2])
#         print(all_global_vars['other_info'])
#         print(all_global_vars['time_stamp'])
#         print(all_global_vars)
#         all_info.objects.create(
#             date_id = int(time.time()),
#             answer = json.dumps(output),
#             name = all_global_vars['user_name'][:-2],
#             other_info = json.dumps(all_global_vars['other_info']),
#             time_stamp = json.dumps(all_global_vars['time_stamp'])
#         )
#     except Exception as e:
#         print(e)
#         try:
#             f = open('C:/workshop/' + str(int(time.time())) + '.txt','w')
#             f.writelines(all_global_vars['user_name'][:-2])
#             f.writelines(';')  
#             for i in all_global_vars['other_info']:
#                 f.writelines(i)    
#                 f.writelines(',')  
#             f.writelines(';')  
#             for i in all_global_vars['videos_answer']:
#                 f.writelines(i)
#                 f.writelines(',')  
#             f.writelines(';')  
#             for i in time_stamp:
#                 f.writelines(i)  
#                 f.writelines(',')  
#             f.close()
#         except Exception as e:
#             print('save falied')
#             print(e)    
#     try:
#         all_info.objects.create(
#                 date_id = int(time.time()),
#                 name = user_name[:-2],
#                 other_info = json.dumps(other_info),
#                 time_stamp = json.dumps(time_stamp)
#         )
#     except Exception as e:
#         print(e)
            
#     return render (request,'learn/results.html',{'name':all_global_vars['user_name'] , 'smile':smile_accuracy , 
#                    'anger': anger_accuracy , 'fear': fear_accuracy , 'surprise':suprise_accuracy,
#                    'smile_ave' : smile_accuracy_average , 'anger_ave': anger_accuracy_average ,
#                    'fear_ave' : fear_accuracy_average , 'surprise_ave': suprise_accuracy_average } )
    
# def save_to_local(data,time_id):
#     #save all global vars to a local .txt file
#     #in case the database crash or other users are unfamiliar with sql
#     #file name is the primary key 
#     #first row is personal info 
#     file_name = 'data/'+ str(time_id) + '.txt'
    
#     personal_info = [data['user_name'], data['personal_info']['uid'],data['personal_info']['gender'], data['personal_info']['age'],
#                      data['personal_info']['glass'],data['personal_info']['ethnicity'], data['personal_info']['language'] , data['erq_answer'],
#                     'only one emotion' if data['is_full_dataset'] else 'all emotions mixed up id is: ' + str (all_global_vars['other_vars']['session_id']) ]

#     verbal_response = list(itertools.zip_longest(data['video_sequence'] , data['videos_answer'], data['time_stamp'], 
#                                                  data['seen_before'],data['confidence'], fillvalue=''))
#     with open (file_name , 'w') as f:
#         for i in personal_info:
#             i = str(i)
#             f.write(i)
#             f.write(',')
#         f.write('\n')
#         for i in verbal_response:
#             for j in i:
#                 f.write(j)
#                 f.write(',')
#             f.write(';')
#             f.write('\n')


# def databasedisplay(request):
#     # dataset admin page, to show the data stored in the database
#     record = all_info.objects.all()
#     print(record)
#     return render(request,'learn/datadisplay.html',{'records':record})



# def save_authenticity (request):
#     global all_global_vars
#     if request.method =='POST':
#         authenticity = request.POST["authenticity"]
# #        global videos_answer
#         all_global_vars['videos_answer'][-1].append(authenticity)
# #        global time_stamp
#         all_global_vars['time_stamp'].append('-')
#         all_global_vars['time_stamp'].append(str(int(time.time())))        
#         print(all_global_vars['videos_answer'])       
# #        if request.POST.has_key('continue'):
#         global skip_confidence 
#         if skip_confidence:
#             return HttpResponseRedirect('emojudgement_playing')
#         return HttpResponseRedirect('emojudgement_confidence')

# def confidence (request):
#     #render the condifence html file 
#     return render (request, 'learn/confidence.html')
    
# def savecondifence (request):
#     # save the condifence data and redirect to playing.html 
#     # check point, for per 20 videos, user can decide to save and quit or continue 
#     # that is for each 20 videos, jump the choice page.
# #    global seen_before , confidence
#     global all_global_vars
#     if request.method =='POST':
#         con_level = request.POST["Option"]
#         seen = request.POST["Unknown/Known"]
#         all_global_vars['seen_before'].append(seen)
#         all_global_vars['confidence'].append(con_level)
#     keys = list(request.POST)
#     print(keys)
#     if 'continue' in keys:   
# #        return HttpResponseRedirect('/confidence')
#         pass
#     if 'end' in keys:
#         print('the second button do something')
#         return HttpResponseRedirect('emojudgement_survey2')
#     normal = 1 
#     global current_page , emo_sequence
#     print(all_global_vars['current_page'])
#     if (all_global_vars['current_page']) == 20:
#         normal = 0
#     if (all_global_vars['current_page']) == 40:
#         normal = 0
#         emo_sequence.pop(0)
#     if (all_global_vars['current_page']) == 60:
#         normal = 0
#         emo_sequence.pop(0)
#     if normal:
#         return HttpResponseRedirect('emojudgement_playing') 
#     else:
#         return HttpResponseRedirect('emojudgement_continueskip') 


# def demo_page(request):
#     return render(request,'learn/Demop.html')

# def info_p1(request):
#     return render(request, 'learn/survey1.html')

# def p4(request):
#     return render(request,'learn/p004.html')

# def info_p1_collection(request):
#     global all_global_vars 
#     if request.method =='POST':            
#         all_global_vars['user_name'] = request.POST["Name"] if request.POST["Name"] else 'User'
#         age = request.POST["Age"]
#         gender = request.POST["Gender"]
#         glass = request.POST["GlassesContacts"]
#         all_global_vars['other_info'].append(age)
#         all_global_vars['other_info'].append(gender)
#         all_global_vars['other_info'].append(glass)
#         all_global_vars['personal_info']['age'] = age
#         all_global_vars['personal_info']['gender'] = gender
#         all_global_vars['personal_info']['glass'] = glass
#     return HttpResponseRedirect('emojudgement_p004')       
    
    
#def test_page(request):
    
    
#def databasedisplay(request):
#    record = all_info.objects.all()
##    record1 = all_info.objects.filter(name = 'test')
#    xx = []
#    for i in record:
#        x = []
#        x.append(i.date_id)
#        x.append(i.name)
#        x.append(i.other_info)
##        x.append(json.loads (i['answer']))
#        x.append(json.loads (i.answer))
#        
#        xx.append(x)
#    print(xx)
#    return HttpResponse (xx)

#def table(request):
#    table_form=forms.SignupForm()   #样式 ，在forms.py里配置好了
#    record = all_info.objects.all()
#    return render_to_response("learn/datadisplay.html",locals()) #必须用这个return
    
#    return HttpResponse('Happiness correct: ' + str(output['h_correct']) + ' out of '+ str(output['h_count']) + '\n ')
#                        'Fear correct: ' + str(f_correct) + ' out of '+ str(f_count) + '\n ' +
#                        'Anger correct: ' + str(a_correct) + ' out of '+ str(a_count) + '\n ' +
#                        'Surprise correct: ' + str(s_correct) + ' out of '+ str(s_count) )
                

#def old_add2_redirect(request, a, b):
#    return HttpResponseRedirect(
#        reverse('add2', args=(a, b))
#    )
#    
#def add(request):
#    a = request.GET['a']
#    b = request.GET['b']
#    c = int(a)+int(b)
#    return HttpResponse(str(c))
#
#def add2(request, a, b):
#    c = int(a) + int(b)
#    return HttpResponse(str(c))

#def result_display(request):
#    #analyze and display the result
##    return HttpResponse('hello world')
#    global videos_answer
#    print(videos_answer)
#    h_count = f_count = a_count = s_count = 0
#    h_correct = f_correct = a_correct = s_correct = 0
#    for emo,actual,answer in videos_answer:
#        if emo == 'H':
#            h_count += 1 
#            if actual == answer:
#                h_correct += 1              
#        elif emo == 'F':
#            f_count += 1
#            if actual == answer:
#                f_correct += 1 
#        elif emo == 'A':
#            a_count += 1
#            if actual == answer:
#                a_correct += 1
#        elif emo == 'S':
#            s_count += 1
#            if actual == answer:
#                s_correct += 1 
#                
#    return HttpResponse('Happiness correct: ' + str(h_correct) + ' out of '+ str(h_count) + '\n ' +
#                        'Fear correct: ' + str(f_correct) + ' out of '+ str(f_count) + '\n ' +
#                        'Anger correct: ' + str(a_correct) + ' out of '+ str(a_count) + '\n ' +
#                        'Surprise correct: ' + str(s_correct) + ' out of '+ str(s_count) )
    

#                
##    context = { 'list_var' : videos_answer }
##    return render (request, 'learn/list.html', context)
#def info_collection_p1(request):
#    if request.method == 'POST':
#        # create a form instance and populate it with data from the request:
#        form = Info1(request.POST)
#        # check whether it's valid:
#        global other_info
#        if form.is_valid():
#            age = form.cleaned_data['Age']
#            sex = form.cleaned_data['sex']
#            glasses = form.cleaned_data['glasses']
#            # process the data in form.cleaned_data as required
#            # ...
#            # redirect to a new URL:
##            return HttpResponse([sex,glasses])
#            
#            other_info.append(sex)
#            other_info.append(glasses)
#            print(other_info)
#            return HttpResponseRedirect('emojudgement_info2')
#
#    # if a GET (or any other method) we'll create a blank form
#    else:
#        form = Info1()
#
#    return render(request, 'learn/survey1.html', {'form': form})