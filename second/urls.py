﻿"""second URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path , include
from learn import views as learn_views

urlpatterns = [
    path('index', learn_views.index, name='emo_index'),
    path('intro', learn_views.intro_page, name='intro'),
    path('tips', learn_views.intro_page2, name='intro2'),
    path('questionnaire',learn_views.questionnaire, name='questionnaire'),
    path('playing', learn_views.playing, name='playing'),
    path('skip',learn_views.breakpage, name = 'skip'),
    path('get_ajex/', learn_views.get_ajex, name ='get_ajex'),
    path('break', learn_views.breakpage, name ='break'),
    path('erq', learn_views.erq, name ='erq'),
    path('save_confidence/', learn_views.save_confidence, name ='save_confidence'),
    path('music_questions', learn_views.music_questions, name ='music_questions'),
    path('music_post_question', learn_views.music_post_question, name ='music_post_question'),
    path('end', learn_views.end, name ='end'),
    path('test', learn_views.test, name ='test'),

]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns

